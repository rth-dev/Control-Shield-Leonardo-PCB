EESchema Schematic File Version 2
LIBS:mySchematicSymbols
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ControlShield-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "ControlShield "
Date "2017-10-20"
Rev "C"
Comp "rth. engineering"
Comment1 "© 2017"
Comment2 ""
Comment3 "Arduino compatible shield"
Comment4 "ControlShield for use with the control demonstrator"
$EndDescr
$Comp
L POT_TRIM RV2
U 1 1 59C8136B
P 5650 4810
F 0 "RV2" V 5475 4810 50  0000 C CNN
F 1 "P_10k" V 5550 4810 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Triwood_RM-065" H 5650 4810 50  0001 C CNN
F 3 "" H 5650 4810 50  0001 C CNN
	1    5650 4810
	0    1    1    0   
$EndComp
$Comp
L POT_TRIM RV3
U 1 1 59C8159C
P 6350 4810
F 0 "RV3" V 6175 4810 50  0000 C CNN
F 1 "I_10k" V 6250 4810 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Triwood_RM-065" H 6350 4810 50  0001 C CNN
F 3 "" H 6350 4810 50  0001 C CNN
	1    6350 4810
	0    1    1    0   
$EndComp
$Comp
L POT_TRIM RV4
U 1 1 59C815CA
P 7050 4810
F 0 "RV4" V 6875 4810 50  0000 C CNN
F 1 "D_10k" V 6950 4810 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Triwood_RM-065" H 7050 4810 50  0001 C CNN
F 3 "" H 7050 4810 50  0001 C CNN
	1    7050 4810
	0    1    1    0   
$EndComp
$Comp
L POT_TRIM RV1
U 1 1 59C815F7
P 4950 4810
F 0 "RV1" V 4775 4810 50  0000 C CNN
F 1 "SPD_10k" V 4850 4810 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Triwood_RM-065" H 4950 4810 50  0001 C CNN
F 3 "" H 4950 4810 50  0001 C CNN
	1    4950 4810
	0    1    1    0   
$EndComp
$Comp
L SW_SPDT SW2
U 1 1 59C8191F
P 7200 1810
F 0 "SW2" H 7200 1980 50  0000 C CNN
F 1 "Intern/Remote" H 7200 1610 50  0000 C CNN
F 2 "myButtons:Switch_Slide_SPDT" H 7200 1810 50  0001 C CNN
F 3 "http://www.adafruit.com/datasheets/EG1218draw.pdf" H 7200 1810 50  0001 C CNN
	1    7200 1810
	-1   0    0    -1  
$EndComp
$Comp
L SW_SPDT SW3
U 1 1 59C81A1A
P 7200 2410
F 0 "SW3" H 7200 2580 50  0000 C CNN
F 1 "Auto/Manual" H 7200 2210 50  0000 C CNN
F 2 "myButtons:Switch_Slide_SPDT" H 7200 2410 50  0001 C CNN
F 3 "http://www.adafruit.com/datasheets/EG1218draw.pdf" H 7200 2410 50  0001 C CNN
	1    7200 2410
	-1   0    0    -1  
$EndComp
$Comp
L Conn_Arduino_Header_Digital_H J10
U 1 1 59CABCD2
P 9800 2710
F 0 "J10" H 9800 3210 50  0000 C CNN
F 1 "Digital H" H 9800 2110 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x10_Pitch2.54mm" H 9800 2710 50  0001 C CNN
F 3 "" H 9800 2710 50  0001 C CNN
	1    9800 2710
	1    0    0    -1  
$EndComp
$Comp
L Conn_Arduino_Header_Analog_In J6
U 1 1 59CACA56
P 2400 4810
F 0 "J6" H 2400 5110 50  0000 C CNN
F 1 "Analog In" H 2400 4410 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x06_Pitch2.54mm" H 2400 4810 50  0001 C CNN
F 3 "" H 2400 4810 50  0001 C CNN
	1    2400 4810
	1    0    0    -1  
$EndComp
$Comp
L Conn_Arduino_Header_Power J4
U 1 1 59CACF6C
P 2400 2810
F 0 "J4" H 2400 3210 50  0000 C CNN
F 1 "Power" H 2400 2310 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x08_Pitch2.54mm" H 2400 2810 50  0001 C CNN
F 3 "" H 2400 2810 50  0001 C CNN
	1    2400 2810
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x08_Male J9
U 1 1 59CBF4EC
P 8100 4810
F 0 "J9" H 8100 4310 50  0000 C CNN
F 1 "Digital_L_Male" H 8100 5210 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x08_Pitch2.54mm" H 8100 4810 50  0001 C CNN
F 3 "" H 8100 4810 50  0001 C CNN
	1    8100 4810
	1    0    0    1   
$EndComp
$Comp
L Conn_Arduino_Header_Digital_L J8
U 1 1 59CBF706
P 9800 4710
F 0 "J8" H 9800 5110 50  0000 C CNN
F 1 "Digital L" H 9800 4210 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x08_Pitch2.54mm" H 9800 4710 50  0001 C CNN
F 3 "" H 9800 4710 50  0001 C CNN
	1    9800 4710
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x06_Male J7
U 1 1 59CC776F
P 3900 4810
F 0 "J7" H 3900 5110 50  0000 C CNN
F 1 "Analog_In_Male" H 3900 4410 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm" H 3900 4810 50  0001 C CNN
F 3 "" H 3900 4810 50  0001 C CNN
	1    3900 4810
	-1   0    0    -1  
$EndComp
$Comp
L Conn_01x08_Male J5
U 1 1 59CC7B46
P 3900 2810
F 0 "J5" H 3900 3210 50  0000 C CNN
F 1 "Power_Male" H 3900 2310 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x08_Pitch2.54mm" H 3900 2810 50  0001 C CNN
F 3 "" H 3900 2810 50  0001 C CNN
	1    3900 2810
	-1   0    0    -1  
$EndComp
$Comp
L Conn_01x10_Male J11
U 1 1 59CC7EC6
P 8100 2810
F 0 "J11" H 8100 2210 50  0000 C CNN
F 1 "Digital_H_Male" H 8100 3310 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x10_Pitch2.54mm" H 8100 2810 50  0001 C CNN
F 3 "" H 8100 2810 50  0001 C CNN
	1    8100 2810
	1    0    0    1   
$EndComp
$Comp
L R R8
U 1 1 59CCD346
P 8350 3610
F 0 "R8" V 8430 3610 50  0000 C CNN
F 1 "1k" V 8350 3610 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 8280 3610 50  0001 C CNN
F 3 "" H 8350 3610 50  0001 C CNN
	1    8350 3610
	0    1    1    0   
$EndComp
$Comp
L R R9
U 1 1 59CCD455
P 8350 3810
F 0 "R9" V 8430 3810 50  0000 C CNN
F 1 "1k" V 8350 3810 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 8280 3810 50  0001 C CNN
F 3 "" H 8350 3810 50  0001 C CNN
	1    8350 3810
	0    1    1    0   
$EndComp
$Comp
L R R3
U 1 1 59CCEB58
P 6900 2960
F 0 "R3" V 6980 2960 50  0000 C CNN
F 1 "10k" V 6900 2960 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6830 2960 50  0001 C CNN
F 3 "" H 6900 2960 50  0001 C CNN
	1    6900 2960
	-1   0    0    1   
$EndComp
$Comp
L R R4
U 1 1 59CD56EE
P 6130 2960
F 0 "R4" V 6210 2960 50  0000 C CNN
F 1 "10k" V 6130 2960 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6060 2960 50  0001 C CNN
F 3 "" H 6130 2960 50  0001 C CNN
	1    6130 2960
	-1   0    0    1   
$EndComp
$Comp
L R R5
U 1 1 59CD5758
P 5930 2960
F 0 "R5" V 6010 2960 50  0000 C CNN
F 1 "10k" V 5930 2960 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5860 2960 50  0001 C CNN
F 3 "" H 5930 2960 50  0001 C CNN
	1    5930 2960
	-1   0    0    1   
$EndComp
NoConn ~ 5100 1610
NoConn ~ 5100 2310
$Comp
L Conn_AVR_ICSP J1
U 1 1 59CFE63F
P 3400 1810
F 0 "J1" H 3400 2010 50  0000 C CNN
F 1 "ICSP back" H 3400 1610 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_2x03_Pitch2.54mm" H 3460 1810 50  0001 C CNN
F 3 "" H 3460 1810 50  0001 C CNN
	1    3400 1810
	1    0    0    -1  
$EndComp
$Comp
L SW_Push_DTL_LED SW4
U 1 1 59D18051
P 4900 1710
F 0 "SW4" H 4900 1880 50  0000 C CNN
F 1 "SW left" H 4900 1335 50  0000 C CNN
F 2 "myButtons:Switch_Push_Mini_LED_SPDT" H 4900 1710 50  0001 C CNN
F 3 "" H 4900 1710 50  0001 C CNN
	1    4900 1710
	1    0    0    -1  
$EndComp
$Comp
L SW_Push_DTL_LED SW5
U 1 1 59D197D0
P 4900 2410
F 0 "SW5" H 4900 2580 50  0000 C CNN
F 1 "SW right" H 4900 2035 50  0000 C CNN
F 2 "myButtons:Switch_Push_Mini_LED_SPDT" H 4900 2410 50  0001 C CNN
F 3 "" H 4900 2410 50  0001 C CNN
	1    4900 2410
	1    0    0    -1  
$EndComp
$Comp
L R R6
U 1 1 59D1A8F5
P 5310 1910
F 0 "R6" V 5390 1910 50  0000 C CNN
F 1 "330" V 5310 1910 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5240 1910 50  0001 C CNN
F 3 "" H 5310 1910 50  0001 C CNN
	1    5310 1910
	0    1    1    0   
$EndComp
$Comp
L R R7
U 1 1 59D1E672
P 5310 2610
F 0 "R7" V 5390 2610 50  0000 C CNN
F 1 "330" V 5310 2610 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5240 2610 50  0001 C CNN
F 3 "" H 5310 2610 50  0001 C CNN
	1    5310 2610
	0    1    1    0   
$EndComp
$Comp
L GND #PWR01
U 1 1 59D2BE8D
P 3270 3270
F 0 "#PWR01" H 3270 3020 50  0001 C CNN
F 1 "GND" H 3270 3120 50  0000 C CNN
F 2 "" H 3270 3270 50  0001 C CNN
F 3 "" H 3270 3270 50  0001 C CNN
	1    3270 3270
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 59D2CC43
P 1880 6720
F 0 "#PWR02" H 1880 6470 50  0001 C CNN
F 1 "GND" H 1880 6570 50  0000 C CNN
F 2 "" H 1880 6720 50  0001 C CNN
F 3 "" H 1880 6720 50  0001 C CNN
	1    1880 6720
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG03
U 1 1 59D2CC87
P 1880 6570
F 0 "#FLG03" H 1880 6645 50  0001 C CNN
F 1 "PWR_FLAG" H 1880 6720 50  0000 C CNN
F 2 "" H 1880 6570 50  0001 C CNN
F 3 "" H 1880 6570 50  0001 C CNN
	1    1880 6570
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR04
U 1 1 59D2D4EE
P 3270 2420
F 0 "#PWR04" H 3270 2270 50  0001 C CNN
F 1 "+5V" H 3270 2560 50  0000 C CNN
F 2 "" H 3270 2420 50  0001 C CNN
F 3 "" H 3270 2420 50  0001 C CNN
	1    3270 2420
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR05
U 1 1 59D2DE83
P 980 6570
F 0 "#PWR05" H 980 6420 50  0001 C CNN
F 1 "+5V" H 980 6710 50  0000 C CNN
F 2 "" H 980 6570 50  0001 C CNN
F 3 "" H 980 6570 50  0001 C CNN
	1    980  6570
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG06
U 1 1 59D2E4EE
P 980 6720
F 0 "#FLG06" H 980 6795 50  0001 C CNN
F 1 "PWR_FLAG" H 980 6870 50  0000 C CNN
F 2 "" H 980 6720 50  0001 C CNN
F 3 "" H 980 6720 50  0001 C CNN
	1    980  6720
	-1   0    0    1   
$EndComp
$Comp
L +3.3V #PWR07
U 1 1 59D2EB12
P 3480 2420
F 0 "#PWR07" H 3480 2270 50  0001 C CNN
F 1 "+3.3V" H 3480 2560 50  0000 C CNN
F 2 "" H 3480 2420 50  0001 C CNN
F 3 "" H 3480 2420 50  0001 C CNN
	1    3480 2420
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR08
U 1 1 59D30A2F
P 1450 6570
F 0 "#PWR08" H 1450 6420 50  0001 C CNN
F 1 "+3.3V" H 1450 6710 50  0000 C CNN
F 2 "" H 1450 6570 50  0001 C CNN
F 3 "" H 1450 6570 50  0001 C CNN
	1    1450 6570
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG09
U 1 1 59D30D3A
P 1450 6720
F 0 "#FLG09" H 1450 6795 50  0001 C CNN
F 1 "PWR_FLAG" H 1450 6870 50  0000 C CNN
F 2 "" H 1450 6720 50  0001 C CNN
F 3 "" H 1450 6720 50  0001 C CNN
	1    1450 6720
	-1   0    0    1   
$EndComp
Text Label 2800 1710 0    39   ~ 0
MISO
Text Label 2800 1810 0    39   ~ 0
SCK
Text Label 4000 1810 2    39   ~ 0
MOSI
Text Label 2800 1910 0    39   ~ 0
RST
Text Label 9300 2510 0    39   ~ 0
AREF
Text Label 9300 2610 0    39   ~ 0
GND
Text Label 9300 4810 0    39   ~ 0
I²C-SCL
Text Label 9300 4910 0    39   ~ 0
I²C-SDA
Text Label 9300 5010 0    39   ~ 0
TTL-Tx
Text Label 9300 5110 0    39   ~ 0
TTL-Rx
Text Label 9300 2910 0    39   ~ 0
LED_left
Text Label 9300 3010 0    39   ~ 0
LED_right
Text Label 9300 3110 0    39   ~ 0
SW_left
Text Label 9300 3210 0    39   ~ 0
SW_right
Text Label 9300 4410 0    39   ~ 0
SW_A/M
Text Label 9300 4510 0    39   ~ 0
SW_I/E
Text Label 2600 3010 0    39   ~ 0
GND
Text Label 2600 3110 0    39   ~ 0
GND
Text Label 2600 3210 0    39   ~ 0
Vin
Text Label 2600 2810 0    39   ~ 0
3.3V
Text Label 2600 2910 0    39   ~ 0
5V
Text Label 2600 2710 0    39   ~ 0
RST
Text Label 2600 2610 0    39   ~ 0
IOREF
Text Label 2600 4610 0    39   ~ 0
TDI
Text Label 2600 4710 0    39   ~ 0
TDO
Text Label 2600 4810 0    39   ~ 0
TMS
Text Label 2600 4910 0    39   ~ 0
TCK
$Comp
L Conn_AVR_ICSP J2
U 1 1 59D52090
P 3400 1110
F 0 "J2" H 3400 1310 50  0000 C CNN
F 1 "ICSP top" H 3400 910 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03_Pitch2.54mm" H 3460 1110 50  0001 C CNN
F 3 "" H 3460 1110 50  0001 C CNN
	1    3400 1110
	1    0    0    -1  
$EndComp
Wire Wire Line
	8300 4410 9600 4410
Wire Wire Line
	8300 4510 9600 4510
Wire Wire Line
	8300 4610 9600 4610
Wire Wire Line
	8300 4710 9600 4710
Wire Wire Line
	8300 4810 9600 4810
Wire Wire Line
	8300 4910 9600 4910
Wire Wire Line
	8300 5010 9600 5010
Wire Wire Line
	8300 5110 9600 5110
Wire Wire Line
	2600 4610 3700 4610
Wire Wire Line
	2600 4710 3700 4710
Wire Wire Line
	2600 4810 3700 4810
Wire Wire Line
	2600 4910 3700 4910
Wire Wire Line
	2600 5010 3700 5010
Wire Wire Line
	2600 5110 3700 5110
Wire Wire Line
	2600 2510 3700 2510
Wire Wire Line
	2600 2710 3700 2710
Wire Wire Line
	2600 2810 3700 2810
Wire Wire Line
	2600 2910 3700 2910
Wire Wire Line
	2600 3010 3700 3010
Wire Wire Line
	2600 3110 3700 3110
Wire Wire Line
	2600 3210 3700 3210
Wire Wire Line
	2600 2610 3700 2610
Wire Wire Line
	8300 3210 9600 3210
Wire Wire Line
	8300 3110 9600 3110
Wire Wire Line
	8300 3010 9600 3010
Wire Wire Line
	8300 2910 9600 2910
Wire Wire Line
	8300 2810 9600 2810
Wire Wire Line
	9600 2710 8300 2710
Wire Wire Line
	8300 2610 9600 2610
Wire Wire Line
	9600 2510 8300 2510
Wire Wire Line
	8300 2410 9600 2410
Wire Wire Line
	9600 2310 8300 2310
Wire Wire Line
	6350 5510 6350 4960
Wire Wire Line
	3400 5510 5650 5510
Wire Wire Line
	5650 5510 5650 4960
Connection ~ 3400 5110
Connection ~ 5500 3810
Wire Wire Line
	6900 3110 6900 4810
Connection ~ 6200 3810
Wire Wire Line
	6500 1710 6500 4810
Connection ~ 5800 3710
Wire Wire Line
	7200 3710 7200 4810
Connection ~ 6500 3710
Wire Wire Line
	5100 3710 5100 4810
Connection ~ 5100 3710
Connection ~ 4800 3810
Wire Wire Line
	4950 5410 4950 4960
Connection ~ 3500 5010
Wire Wire Line
	7000 2510 6900 2510
Wire Wire Line
	6900 1910 6900 2810
Wire Wire Line
	7000 1910 6900 1910
Connection ~ 6900 2510
Connection ~ 6500 2310
Wire Wire Line
	5800 3710 5800 4810
Wire Wire Line
	7600 1910 7600 2410
Wire Wire Line
	7600 2410 7400 2410
Connection ~ 3000 3110
Connection ~ 3000 3010
Wire Wire Line
	3100 2910 3100 3710
Connection ~ 3100 2910
Connection ~ 7200 3710
Wire Wire Line
	9000 1810 9000 4510
Connection ~ 9000 4510
Wire Wire Line
	8900 1910 8900 4410
Connection ~ 8900 4410
Wire Wire Line
	8500 3810 8600 3810
Wire Wire Line
	8600 3810 8600 4810
Connection ~ 8600 4810
Wire Wire Line
	8500 3610 8700 3610
Wire Wire Line
	8700 3610 8700 4910
Connection ~ 8700 4910
Wire Wire Line
	8200 3610 8100 3610
Wire Wire Line
	8100 3610 8100 3810
Wire Wire Line
	8100 3810 8200 3810
Connection ~ 8100 3710
Wire Wire Line
	3100 3710 8100 3710
Wire Wire Line
	3000 3810 6900 3810
Connection ~ 6900 3810
Wire Wire Line
	7000 2310 6500 2310
Wire Wire Line
	6500 1710 7000 1710
Wire Wire Line
	4500 2510 4700 2510
Wire Wire Line
	4500 2410 4700 2410
Connection ~ 4500 2510
Connection ~ 4500 2410
Wire Wire Line
	4500 1810 4700 1810
Connection ~ 4500 1810
Wire Wire Line
	4800 4810 4800 3810
Connection ~ 4500 3710
Wire Wire Line
	5930 3210 6130 3210
Wire Wire Line
	6130 3210 6130 3110
Wire Wire Line
	5930 3210 5930 3110
Connection ~ 6030 3210
Wire Wire Line
	8700 3410 8700 3210
Connection ~ 8700 3210
Wire Wire Line
	8600 3110 8600 3310
Connection ~ 8600 3110
Connection ~ 4300 3810
Connection ~ 8700 3010
Connection ~ 8600 2910
Wire Wire Line
	5100 2510 6290 2510
Connection ~ 5930 2510
Wire Wire Line
	5930 2810 5930 2510
Wire Wire Line
	5100 1810 6380 1810
Wire Wire Line
	6130 1810 6130 2810
Connection ~ 6900 3510
Connection ~ 4300 1910
Connection ~ 4500 1710
Wire Wire Line
	3800 1710 4700 1710
Wire Wire Line
	4300 2610 4700 2610
Connection ~ 4300 2610
Wire Wire Line
	4300 1210 4300 3810
Wire Wire Line
	5160 1910 5100 1910
Wire Wire Line
	6380 1810 6380 3310
Connection ~ 6130 1810
Wire Wire Line
	6380 3310 8600 3310
Wire Wire Line
	6290 2510 6290 3410
Wire Wire Line
	6290 3410 8700 3410
Wire Wire Line
	5500 4810 5500 3810
Wire Wire Line
	5160 2610 5100 2610
Wire Wire Line
	5690 2610 5460 2610
Wire Wire Line
	6200 4810 6200 3810
Wire Wire Line
	6030 3210 6030 3810
Connection ~ 6030 3810
Wire Wire Line
	5460 1910 5800 1910
Wire Wire Line
	5800 1910 5800 1410
Wire Wire Line
	5800 1410 8600 1410
Wire Wire Line
	8600 1410 8600 2910
Wire Wire Line
	8700 1310 5690 1310
Wire Wire Line
	8700 1310 8700 3010
Wire Wire Line
	5690 1310 5690 2610
Wire Wire Line
	1880 6570 1880 6720
Wire Wire Line
	980  6570 980  6720
Wire Wire Line
	3270 2420 3270 2910
Connection ~ 3270 2910
Wire Wire Line
	3270 3270 3270 3110
Connection ~ 3270 3110
Wire Wire Line
	3000 3010 3000 3810
Wire Wire Line
	3480 2420 3480 2810
Connection ~ 3480 2810
Wire Wire Line
	1450 6570 1450 6720
Wire Wire Line
	3800 1810 4000 1810
Wire Wire Line
	8800 2610 8800 3510
Connection ~ 8800 2610
Wire Wire Line
	8800 3510 6900 3510
Wire Wire Line
	7400 1810 9000 1810
Wire Wire Line
	8900 1910 7600 1910
Wire Wire Line
	3500 5410 4950 5410
Wire Wire Line
	3800 1910 4700 1910
Wire Wire Line
	3800 1210 4300 1210
Wire Wire Line
	3800 1110 4000 1110
Wire Wire Line
	2800 1910 3000 1910
Wire Wire Line
	3000 1810 2800 1810
Wire Wire Line
	3000 1710 2800 1710
Wire Wire Line
	2800 1110 3000 1110
Wire Wire Line
	3000 1010 2800 1010
Text Label 2800 1010 0    39   ~ 0
MISO
Text Label 2800 1110 0    39   ~ 0
SCK
Text Label 2800 1210 0    39   ~ 0
RST
Wire Wire Line
	4500 1010 4500 3710
Wire Wire Line
	3800 1010 4500 1010
Text Label 4000 1110 2    39   ~ 0
MOSI
Wire Wire Line
	2200 1010 2300 1010
Wire Wire Line
	2300 1010 2300 1210
Connection ~ 2300 1210
Wire Wire Line
	2200 1210 3000 1210
Wire Wire Line
	1700 1010 1500 1010
Wire Wire Line
	1500 1010 1500 1310
Wire Wire Line
	1500 1210 1700 1210
$Comp
L GND #PWR010
U 1 1 59D559F8
P 1500 1310
F 0 "#PWR010" H 1500 1060 50  0001 C CNN
F 1 "GND" H 1500 1160 50  0000 C CNN
F 2 "" H 1500 1310 50  0001 C CNN
F 3 "" H 1500 1310 50  0001 C CNN
	1    1500 1310
	1    0    0    -1  
$EndComp
Connection ~ 1500 1210
$Comp
L SW_Reset SW1
U 1 1 59D58B1A
P 1950 1110
F 0 "SW1" H 1950 1320 50  0000 C CNN
F 1 "Reset" H 1950 940 50  0000 C CNN
F 2 "myButtons:Switch_SMD_Reset" H 1950 1410 50  0001 C CNN
F 3 "" H 1950 1410 50  0001 C CNN
	1    1950 1110
	1    0    0    -1  
$EndComp
$Comp
L Conn_AVR_JTAG J3
U 1 1 59D5956F
P 3610 6660
F 0 "J3" H 3610 6970 50  0000 C CNN
F 1 "JTAG" H 3620 6350 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x05_Pitch2.54mm" H 3410 6660 50  0001 C CNN
F 3 "" H 3410 6660 50  0001 C CNN
	1    3610 6660
	1    0    0    -1  
$EndComp
Wire Wire Line
	4010 6460 4210 6460
Wire Wire Line
	4210 6460 4210 6860
Wire Wire Line
	4210 6860 4010 6860
$Comp
L GND #PWR011
U 1 1 59D5BAD3
P 4210 6860
F 0 "#PWR011" H 4210 6610 50  0001 C CNN
F 1 "GND" H 4210 6710 50  0000 C CNN
F 2 "" H 4210 6860 50  0001 C CNN
F 3 "" H 4210 6860 50  0001 C CNN
	1    4210 6860
	1    0    0    -1  
$EndComp
NoConn ~ 3210 6760
Wire Wire Line
	4010 6660 4510 6660
Text Label 4510 6660 2    39   ~ 0
RST
$Comp
L +5V #PWR012
U 1 1 59D5D4E5
P 4310 6460
F 0 "#PWR012" H 4310 6310 50  0001 C CNN
F 1 "+5V" H 4310 6600 50  0000 C CNN
F 2 "" H 4310 6460 50  0001 C CNN
F 3 "" H 4310 6460 50  0001 C CNN
	1    4310 6460
	1    0    0    -1  
$EndComp
Wire Wire Line
	4010 6560 4310 6560
Wire Wire Line
	4310 6560 4310 6460
Text Label 2910 6460 0    39   ~ 0
TCK
Wire Wire Line
	3210 6460 2910 6460
Wire Wire Line
	3210 6560 2910 6560
Wire Wire Line
	3210 6660 2910 6660
Wire Wire Line
	3210 6860 2910 6860
Text Label 2910 6560 0    39   ~ 0
TDO
Text Label 2910 6660 0    39   ~ 0
TMS
Text Label 2910 6860 0    39   ~ 0
TDI
Text Label 2600 5010 0    39   ~ 0
RV_SPD
Text Label 2600 5110 0    39   ~ 0
RV_P
Text Label 9300 4710 0    39   ~ 0
RV_I
Text Label 9300 2810 0    39   ~ 0
RV_D
Wire Wire Line
	3500 5010 3500 5410
Wire Wire Line
	3400 5110 3400 5510
Wire Wire Line
	6350 5510 9225 5510
Wire Wire Line
	9225 5510 9225 4710
Connection ~ 9225 4710
Wire Wire Line
	7050 4960 7050 5410
Wire Wire Line
	7050 5410 9125 5410
Wire Wire Line
	9125 5410 9125 2810
Connection ~ 9125 2810
NoConn ~ 4010 6760
Text Label 9300 2710 0    39   ~ 0
LED2
Text Label 9300 4610 0    39   ~ 0
LED1
$Comp
L LED D1
U 1 1 59D65F14
P 5550 6400
F 0 "D1" H 5550 6500 50  0000 C CNN
F 1 "LED1" H 5550 6300 50  0000 C CNN
F 2 "LEDs:LED_D3.0mm" H 5550 6400 50  0001 C CNN
F 3 "" H 5550 6400 50  0001 C CNN
	1    5550 6400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR013
U 1 1 59D66075
P 5200 6900
F 0 "#PWR013" H 5200 6650 50  0001 C CNN
F 1 "GND" H 5200 6750 50  0000 C CNN
F 2 "" H 5200 6900 50  0001 C CNN
F 3 "" H 5200 6900 50  0001 C CNN
	1    5200 6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 6400 5200 6900
$Comp
L LED D2
U 1 1 59D68140
P 5550 6700
F 0 "D2" H 5550 6800 50  0000 C CNN
F 1 "LED2" H 5550 6600 50  0000 C CNN
F 2 "LEDs:LED_D3.0mm" H 5550 6700 50  0001 C CNN
F 3 "" H 5550 6700 50  0001 C CNN
	1    5550 6700
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 59D6885E
P 5950 6400
F 0 "R1" V 6030 6400 50  0000 C CNN
F 1 "330" V 5950 6400 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5880 6400 50  0001 C CNN
F 3 "" H 5950 6400 50  0001 C CNN
	1    5950 6400
	0    1    1    0   
$EndComp
$Comp
L R R2
U 1 1 59D68DA5
P 5950 6700
F 0 "R2" V 6030 6700 50  0000 C CNN
F 1 "330" V 5950 6700 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5880 6700 50  0001 C CNN
F 3 "" H 5950 6700 50  0001 C CNN
	1    5950 6700
	0    1    1    0   
$EndComp
Text Label 6300 6400 2    39   ~ 0
LED1
Text Label 6300 6700 2    39   ~ 0
LED2
Connection ~ 5200 6700
Wire Wire Line
	5200 6400 5400 6400
Wire Wire Line
	5200 6700 5400 6700
Wire Wire Line
	5700 6700 5800 6700
Wire Wire Line
	5800 6400 5700 6400
Wire Wire Line
	6100 6400 6300 6400
Wire Wire Line
	6300 6700 6100 6700
$EndSCHEMATC
