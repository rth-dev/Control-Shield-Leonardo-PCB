# ControlShield
an arduino leonardo control shield

![PCB Rev C front](https://gitlab.com/rth-dev/Control-Shield-Leonardo-PCB/-/raw/master/revisions/Rev_C/ControlShield%20Rev_C%20front.png)

![PCB Rev C back](https://gitlab.com/rth-dev/Control-Shield-Leonardo-PCB/-/raw/master/revisions/Rev_C/ControlShield%20Rev_C%20back.png)